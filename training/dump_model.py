from retinanet import *
import numpy as np
import logging
logging.basicConfig(format='[%(levelname)s] %(message)s', level=logging.DEBUG)

import os

from tensorflow_model_optimization.quantization.keras import vitis_quantize


model_path = "quantized/quantized_pqt.h5"
dump_dir = "dump_results/out"
if not os.path.isdir(dump_dir):
  logging.warn("%s doesn't exists. Creating it.", dump_dir)
  os.makedirs(dump_dir)

logging.info("Loading model: {}".format(model_path))
model = tf.keras.models.load_model(model_path, custom_objects={"RetinaNetLoss": RetinaNetLoss}, compile=False)


"""
## Load the Dataset
"""
logging.info("Loading dataset")
(train_dataset, val_dataset), dataset_info = tfds.load(
   "coco/2017", split=["train", "validation"], with_info=True, data_dir="coco", download=True
)

def preprocess_data_1(sample):
    """Applies preprocessing step to a single sample

    Arguments:
      sample: A dict representing a single training sample.

    Returns:
      image: Resized and padded image with random horizontal flipping applied.
      bbox: Bounding boxes with the shape `(num_objects, 4)` where each box is
        of the format `[x, y, width, height]`.
      class_id: An tensor representing the class id of the objects, having
        shape `(num_objects,)`.
    """
    image = sample["image"]
    bbox = swap_xy(sample["objects"]["bbox"])
    class_id = tf.cast(sample["objects"]["label"], dtype=tf.int32)

    # image, bbox = random_flip_horizontal(image, bbox)
    image, image_shape, _ = resize_and_pad_image(image,
                                                min_side=300,
                                                max_side=300,
                                                jitter=None)
    
    bbox = tf.stack(
        [
            bbox[:, 0] * image_shape[1],
            bbox[:, 1] * image_shape[0],
            bbox[:, 2] * image_shape[1],
            bbox[:, 3] * image_shape[0],
        ],
        axis=-1,
    )
    bbox = convert_to_xywh(bbox)
    return image, bbox, class_id

logging.debug("Preprocessing dataset")
autotune = tf.data.AUTOTUNE
label_encoder = LabelEncoder()
# batch_size = 50
# train_dataset = train_dataset.map(preprocess_data, num_parallel_calls=autotune)
# train_dataset = train_dataset.shuffle(8 * batch_size)
# train_dataset = train_dataset.padded_batch(
#     batch_size=batch_size, padding_values=(0.0, 1e-8, -1), drop_remainder=True
# )
# train_dataset = train_dataset.map(
#     label_encoder.encode_batch, num_parallel_calls=autotune
# )
# train_dataset = train_dataset.apply(tf.data.experimental.ignore_errors())
# train_dataset = train_dataset.prefetch(autotune)

val_dataset = val_dataset.map(preprocess_data, num_parallel_calls=autotune)
val_dataset = val_dataset.padded_batch(
    batch_size=1, padding_values=(0.0, 1e-8, -1), drop_remainder=True
)
val_dataset = val_dataset.map(label_encoder.encode_batch, num_parallel_calls=autotune)
val_dataset = val_dataset.apply(tf.data.experimental.ignore_errors())
val_dataset = val_dataset.prefetch(autotune)

"""
## Dump model
"""

logging.info("Dumping model: {}".format(model_path))

from tensorflow_model_optimization.quantization.keras import vitis_quantize
with vitis_quantize.quantize_scope():
    quantized_model = keras.models.load_model(model_path)
    vitis_quantize.VitisQuantizer.dump_model(
      model = quantized_model, 
      dataset = val_dataset, 
      output_dir=dump_dir)

logging.info("Dumping results saved to {}".format(dump_dir))