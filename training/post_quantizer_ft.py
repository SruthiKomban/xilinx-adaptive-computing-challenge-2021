from retinanet import *
import numpy as np

import os

model_path = "Aveleda_SGD/float/float.h5"
quantized_dir = "Aveleda_SQD/quantized"
if not os.path.isdir(quantized_dir):
   os.makedirs(quantized_dir)


model = tf.keras.models.load_model(model_path, custom_objects={"RetinaNetLoss": RetinaNetLoss}, compile=False)


"""
## Load the Dataset
"""
print("[INFO] Loading dataset")
(train_dataset, val_dataset), dataset_info = tfds.load(
   "aveleda", split=["train", "validation"], with_info=True, data_dir="aveleda", download=True
)

def preprocess_data_1(sample):
    """Applies preprocessing step to a single sample

    Arguments:
      sample: A dict representing a single training sample.

    Returns:
      image: Resized and padded image with random horizontal flipping applied.
      bbox: Bounding boxes with the shape `(num_objects, 4)` where each box is
        of the format `[x, y, width, height]`.
      class_id: An tensor representing the class id of the objects, having
        shape `(num_objects,)`.
    """
    image = sample["image"]
    bbox = swap_xy(sample["objects"]["bbox"])
    class_id = tf.cast(sample["objects"]["label"], dtype=tf.int32)

    # image, bbox = random_flip_horizontal(image, bbox)
    image, image_shape, _ = resize_and_pad_image(image,
                                                min_side=300,
                                                max_side=300,
                                                jitter=None)
    
    bbox = tf.stack(
        [
            bbox[:, 0] * image_shape[1],
            bbox[:, 1] * image_shape[0],
            bbox[:, 2] * image_shape[1],
            bbox[:, 3] * image_shape[0],
        ],
        axis=-1,
    )
    bbox = convert_to_xywh(bbox)
    return image, bbox, class_id

print("[INFO] Preprocessing dataset")
autotune = tf.data.AUTOTUNE
label_encoder = LabelEncoder()
batch_size = 8
train_dataset = train_dataset.map(preprocess_data, num_parallel_calls=autotune)
train_dataset = train_dataset.shuffle(8 * batch_size)
train_dataset = train_dataset.padded_batch(
    batch_size=batch_size, padding_values=(0.0, 1e-8, -1), drop_remainder=True
)
train_dataset = train_dataset.map(
    label_encoder.encode_batch, num_parallel_calls=autotune
)
train_dataset = train_dataset.apply(tf.data.experimental.ignore_errors())
train_dataset = train_dataset.prefetch(autotune)

# val_dataset = val_dataset.map(preprocess_data_1, num_parallel_calls=autotune)
# val_dataset = val_dataset.padded_batch(
#     batch_size=1, padding_values=(0.0, 1e-8, -1), drop_remainder=True
# )
# val_dataset = val_dataset.map(label_encoder.encode_batch, num_parallel_calls=autotune)
# val_dataset = val_dataset.apply(tf.data.experimental.ignore_errors())
# val_dataset = val_dataset.prefetch(autotune)

"""
## Quantization
"""

print("[INFO] Post-quantizing the model")
from tensorflow_model_optimization.quantization.keras import vitis_quantize
quantizer = vitis_quantize.VitisQuantizer(model)
quantized_model = quantizer.quantize_model(calib_dataset=train_dataset.take(1000), calib_steps=5, include_fast_ft=True, fast_ft_epochs=10000)

"""
## Save the quantized model
"""
quantized_filename_h5 = os.path.join(quantized_dir, "quantized_pqt_ft.h5")
print("[INFO] Saving HDF5 quantized model at {}".format(quantized_filename_h5))
quantized_model.save(quantized_filename_h5, save_format="h5")
