import sys
import time
import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow_model_optimization.quantization.keras import vitis_quantize

import matplotlib.pyplot as plt
import tensorflow_datasets as tfds

from retinanet import *
import json

from pathlib import Path

import forest_trunks.forest_trunks

"""
## Loading the model
"""
print("[INFO] Loading model")
model_path="Aveleda_SGD/quantized/quantized_qat.h5"
# model_path = "Aveleda_SGD/float/float.h5"
model = tf.keras.models.load_model(model_path, custom_objects={"RetinaNetLoss": RetinaNetLoss}, compile=False)

"""
## Building inference model
"""
print("[INFO] Building inference model")
image = tf.keras.Input(shape=[None, None, 3], name="image")
predictions = model(image, training=False)
detections = DecodePredictions(confidence_threshold=0.5)(image, predictions)
inference_model = tf.keras.Model(inputs=image, outputs=detections)

"""
## Loading Dataset
"""
print("[INFO] Loading datset")

print("[INFO] Loading dataset")
(test_dataset), dataset_info = tfds.load(
   "aveleda", split="test", with_info=True, data_dir="aveleda", download=True
)

"""
## Generating detections
"""


def prepare_image(image):
    image, _, ratio = resize_and_pad_image(image, jitter=None)
    image = tf.keras.applications.resnet.preprocess_input(image)
    return tf.expand_dims(image, axis=0), ratio


int2str = dataset_info.features["objects"]["label"].int2str

results = []
time_ = []

from tqdm import tqdm
for sample in tqdm(test_dataset.take(100)):    
    image = tf.cast(sample["image"], dtype=tf.float32)
    
    itime = time.thread_time()
    
    input_image, ratio = prepare_image(image)
    detections = inference_model.predict(input_image)
    num_detections = detections.valid_detections[0]
    class_ids = detections.nmsed_classes[0][:num_detections]
    class_names = [
        int2str(int(x)) for x in detections.nmsed_classes[0][:num_detections]
    ]

    img_shape = tf.cast(image.shape[:2], dtype=tf.float32)
    img_shapes = tf.concat([img_shape, img_shape], axis=-1)
    bboxes = detections.nmsed_boxes[0][:num_detections] / 384.0  #* img_shapes
    scores = detections.nmsed_scores[0][:num_detections]

    #print(bboxes)

    ftime = time.thread_time()
    time_.append(ftime-itime)
    
    for bbox, score, class_id, class_name in zip(bboxes, scores, class_ids, class_names):
        result_ = {
            "image_id": Path(str(sample["image/filename"].numpy().decode("utf-8"))).stem,
            "category_id": int(class_id),
            "category_name": class_name,
            "bbox": bbox.tolist(),
            "score": float(score)
        }
        results.append(result_)
    # print(results)

avg_time = sum(time_)/len(time_) * 1000
print("[INFO] Average inference time: {avg_time:.2f} ms".format(avg_time=avg_time))

# print(results)
json_results = json.dumps(results, ensure_ascii=False)
with open('Aveleda_SGD/quantized/qat_results.json', 'w') as outfile:
    json.dump(results, outfile)
