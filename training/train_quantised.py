"""
Title: Object Detection with RetinaNet
Author: [Srihari Humbarwadi](https://twitter.com/srihari_rh)
Date created: 2020/05/17
Last modified: 2020/07/14
Description: Implementing RetinaNet: Focal Loss for Dense Object Detection.
"""

"""
## Introduction

Object detection a very important problem in computer
vision. Here the model is tasked with localizing the objects present in an
image, and at the same time, classifying them into different categories.
Object detection models can be broadly classified into "single-stage" and
"two-stage" detectors. Two-stage detectors are often more accurate but at the
cost of being slower. Here in this example, we will implement RetinaNet,
a popular single-stage detector, which is accurate and runs fast.
RetinaNet uses a feature pyramid network to efficiently detect objects at
multiple scales and introduces a new loss, the Focal loss function, to alleviate
the problem of the extreme foreground-background class imbalance.

**References:**

- [RetinaNet Paper](https://arxiv.org/abs/1708.02002)
- [Feature Pyramid Network Paper](https://arxiv.org/abs/1612.03144)
"""


from email.policy import default
from gc import callbacks
import os
from pickletools import optimize

import numpy as np
import tensorflow as tf
from tensorflow import keras

import matplotlib.pyplot as plt
import tensorflow_datasets as tfds

from retinanet import *
import aveleda.aveleda
import forest_trunks.forest_trunks
import keras_tuner as kt

# from callbacks.eval import Evaluate



batch_size = 12
model_dir = "checkpoints/Aveleda"
label_encoder = LabelEncoder()

def read_set (input_file):
    #tf.logging.info("Reading record dataset for input file %s" % input_file)

    filenames = tf.compat.v1.gfile.Glob(input_file)
    filename_set = tf.data.Dataset.from_tensor_slices(filenames)
    return filename_set

def model_builder(hp):
    """
    ## Setting up training parameters
    """ 

    num_classes = 80

    learning_rates = [2.5e-06, 0.000625, 0.00125, 0.0025, 0.00025, 2.5e-05]
    learning_rate_boundaries = [125, 250, 500, 240000, 360000]
    learning_rate_fn = tf.optimizers.schedules.PiecewiseConstantDecay(
        boundaries=learning_rate_boundaries, values=learning_rates
    )

    """
    ## Initializing and compiling model
    """

    alpha = hp.Float('alpha', min_value=0.0, max_value=1.0, default=0.25, sampling='linear')
    gamma = hp.Float('gamma', min_value=0.5, max_value=5.0, default=2.0, sampling='linear')

    loss_fn = RetinaNetLoss(num_classes, alpha=alpha, gamma=gamma)
    model = RetinaNet(num_classes)

    # checkpoints_path = 'checkpoints/Aveleda_SGD'
    latest_checkpoint = 'checkpoints/Aveleda_SGD/retinanet_resnest50_coco_24.h5'
    # assert latest_checkpoint, "[FATAL] Any checkpoint weights available in ".format(checkpoints_path)
    # print('[INFO] Loading weights from {}'.format(latest_checkpoint))
    # model.load_weights(latest_checkpoint).expect_partial()
    model.load_weights(latest_checkpoint)#.expect_partial()

    from tensorflow_model_optimization.quantization.keras import vitis_quantize
    quantizer = vitis_quantize.VitisQuantizer(model)
    qat_model = quantizer.get_qat_model(
        init_quant=True, # Do init PTQ quantization will help us to get a better initial state for the quantizers, especially for `8bit_tqt` strategy. Must be used together with calib_dataset
        calib_dataset=train_dataset,
        calib_steps=10
        )

    hp_learning_rate = hp.Float('learning_rate', min_value=1e-6, max_value=2e-3, sampling='log')
    hp_momentum = hp.Float('momentum', min_value=0.1, max_value=0.9, default=0.9, sampling='linear')

    optimizer = tf.optimizers.SGD(learning_rate=learning_rate_fn, momentum=hp_momentum)
    #optimizer = tf.optimizers.Adam(learning_rate=hp_learning_rate)
    qat_model.compile(loss=loss_fn, 
                  optimizer=optimizer)

    return qat_model

def create_callbacks(model, training_model, prediction_model, validation_generator, args):
    """ Creates the callbacks to use during training.
    Args
        model: The base model.
        training_model: The model that is used for training.
        prediction_model: The model that should be used for validation.
        validation_generator: The generator for creating validation data.
        args: parseargs args object.
    Returns:
        A list of callbacks used for training.
    """
    callbacks = []

    tensorboard_callback = None

    if args["tensorboard_dir"]:
        os.makedirs(args["tensorboard_dir"], exist_ok=True)
        update_freq = args["tensorboard_freq"]
        if update_freq not in ['epoch', 'batch']:
            update_freq = int(update_freq)
        tensorboard_callback = keras.callbacks.TensorBoard(
            log_dir                = args["tensorboard_dir"],
            histogram_freq         = 0,
            batch_size             = args["batch_size"],
            write_graph            = True,
            write_grads            = False,
            write_images           = False,
            update_freq            = update_freq,
            embeddings_freq        = 0,
            embeddings_layer_names = None,
            embeddings_metadata    = None
        )

    print(tf.data.experimental.cardinality(validation_generator))
    if args["evaluation"] and validation_generator:
        if args["dataset_type"] == 'coco':
            from callbacks.coco import CocoEval

            # use prediction model for evaluation
            evaluation = CocoEval(validation_generator, tensorboard=tensorboard_callback)
        else:
            evaluation = Evaluate(validation_generator, tensorboard=tensorboard_callback, weighted_average=args["weighted_average"])
        # evaluation = RedirectModel(evaluation, prediction_model)
        callbacks.append(evaluation)

    # save the model
    if args["snapshots"]:
        # ensure directory created first; otherwise h5py will error after epoch.
        os.makedirs(args["snapshot_path"], exist_ok=True)
        checkpoint = keras.callbacks.ModelCheckpoint(
            os.path.join(
                args["snapshot_path"],
                '{backbone}_{dataset_type}_{{epoch:02d}}.h5'.format(backbone=args["backbone"], dataset_type="aveleda")
            ),
            verbose=1,
            # save_weights_only=True,
            # save_best_only=True,
            # monitor="mAP",
            # mode='max'
        )
        # checkpoint = RedirectModel(checkpoint, model)
        callbacks.append(checkpoint)

    # callbacks.append(keras.callbacks.ReduceLROnPlateau(
    #     monitor    = 'loss',
    #     factor     = args.reduce_lr_factor,
    #     patience   = args.reduce_lr_patience,
    #     verbose    = 1,
    #     mode       = 'auto',
    #     min_delta  = 0.0001,
    #     cooldown   = 0,
    #     min_lr     = 0
    # ))

    if args["evaluation"] and validation_generator:
        callbacks.append(keras.callbacks.EarlyStopping(
            monitor    = 'mAP',
            patience   = 5,
            mode       = 'max',
            min_delta  = 0.01
        ))

    if args["tensorboard_dir"]:
        callbacks.append(tensorboard_callback)

    return callbacks


"""
## Load the Aveleda dataset dataset using TensorFlow Datasets
"""

# set `data_dir=None` to load the complete dataset
(train_dataset, val_dataset), dataset_info = tfds.load(
    "aveleda", split=["train", "validation"], with_info=True, data_dir="aveleda", download=True
)
# print(dataset_info)

# train_dataset = read_set("dataset/aveleda/train.record")
# val_dataset = read_set("dataset/aveleda/eval.record")

"""
## Setting up a `tf.data` pipeline

To ensure that the model is fed with data efficiently we will be using
`tf.data` API to create our input pipeline. The input pipeline
consists for the following major processing steps:

- Apply the preprocessing function to the samples
- Create batches with fixed batch size. Since images in the batch can
have different dimensions, and can also have different number of
objects, we use `padded_batch` to the add the necessary padding to create
rectangular tensors
- Create targets for each sample in the batch using `LabelEncoder`
"""

autotune = tf.data.AUTOTUNE
train_dataset = train_dataset.map(preprocess_data, num_parallel_calls=autotune)
train_dataset = train_dataset.shuffle(8 * batch_size)
train_dataset = train_dataset.padded_batch(
    batch_size=batch_size, padding_values=(0.0, 1e-8, -1), drop_remainder=True
)
train_dataset = train_dataset.map(
    label_encoder.encode_batch, num_parallel_calls=autotune
)
train_dataset = train_dataset.apply(tf.data.experimental.ignore_errors())
train_dataset = train_dataset.prefetch(autotune)

val_dataset = val_dataset.map(preprocess_data, num_parallel_calls=autotune)
val_dataset = val_dataset.padded_batch(
    batch_size=1, padding_values=(0.0, 1e-8, -1), drop_remainder=True
)
val_dataset = val_dataset.map(label_encoder.encode_batch, num_parallel_calls=autotune)
val_dataset = val_dataset.apply(tf.data.experimental.ignore_errors())
val_dataset = val_dataset.prefetch(autotune)


"""
## Setting up callbacks
"""
args = {
    "tensorboard_dir": "logs_vitis/Aveleda_SGD_quantised",
    "tensorboard_freq": "batch",
    "dataset_type": "coco",
    "snapshots": True,
    "snapshot_path": "checkpoints_vitis/Aveleda_SGD_quantised",
    "backbone": "retinanet_resnest50",
    "reduce_lr_factor": 0,
    "reduce_lr_patience": 1000,
    "evaluation": False,
    "batch_size": batch_size,
}

callbacks = create_callbacks(None, None, None, val_dataset, args)


"""
## Tuner
"""
# Uncomment the following lines, when training on full dataset
train_steps_per_epoch = dataset_info.splits["train"].num_examples // batch_size
val_steps_per_epoch = \
    dataset_info.splits["validation"].num_examples // batch_size

train_steps = 2 * 100000
epochs = train_steps // train_steps_per_epoch

tuner = kt.Hyperband(model_builder,
                     objective='val_loss',
                     max_epochs=10,
                     factor=3,
                     directory='tuner_vitis',
                     project_name='Aveleda SGD Quantised Tuning')


tensorboard_callback = tf.keras.callbacks.TensorBoard(
        log_dir                = args["tensorboard_dir"],
        histogram_freq         = 0,
        batch_size             = args["batch_size"],
        write_graph            = True,
        write_grads            = False,
        write_images           = False,
        update_freq            = args["tensorboard_freq"],
        embeddings_freq        = 0,
        embeddings_layer_names = None,
        embeddings_metadata    = None
    )

early_stopping = tf.keras.callbacks.EarlyStopping(
        monitor    = 'val_loss',
        patience   = 5,
        mode       = 'min',
        min_delta  = 0.1
    )

terminate_nan = tf.keras.callbacks.TerminateOnNaN()

tuner_callbacks = [early_stopping, terminate_nan, tensorboard_callback]

tuner.search(
    train_dataset.take(1000),
    validation_data = val_dataset,
    epochs=10,
    callbacks=tuner_callbacks
)

best_hps=tuner.get_best_hyperparameters(num_trials=1)[0]


"""
## Training the model
"""

model = tuner.hypermodel.build(best_hps)
# model = model_builder(None)

# Uncomment the following lines, when training on full dataset
train_steps_per_epoch = dataset_info.splits["train"].num_examples // batch_size
val_steps_per_epoch = \
    dataset_info.splits["validation"].num_examples // batch_size

train_steps = 4 * 100000
epochs = train_steps // train_steps_per_epoch

#epochs = 1

# Running 100 training and 50 validation steps,
# remove `.take` when training on the full dataset

model.fit(
    train_dataset,
    validation_data=val_dataset,
    epochs=200000,
    callbacks=callbacks,
    verbose=1,
)