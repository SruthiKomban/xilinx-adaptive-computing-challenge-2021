# coding=utf-8
# Copyright 2022 The TensorFlow Datasets Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""PASCAL VOC datasets."""

import os
import xml.etree.ElementTree

import tensorflow as tf
import tensorflow_datasets.public_api as tfds

_VOC_CITATION = """\
@dataset{{andre_silva_aguiar_2021_5139598,\
  author       = {{André Silva Aguiar}},\
  title        = {{Grape bunch and vine trunk dataset for Deep \
                   Learning object detection.}},\
  month        = jul,\
  year         = 2021,\
  publisher    = {{Zenodo}},\
  doi          = {{10.5281/zenodo.5139598}},\
  url          = {https://doi.org/10.5281/zenodo.5139598}\
}}\
"""

_VOC_DESCRIPTION = """
Grape bunch and vine trunk dataset containing images and annotations for Deep Learning object detection.
"""

_VOC_CONFIG_DESCRIPTION = """\
This dataset contains Object Detection data from Aveleda vineyards. \
It contains data from grapes in different growing stages and trunks.
"""

_VOC_URL = "https://doi.org/10.5281/zenodo.5139598"
# Original site, it is down very often.
# _VOC_DATA_URL = "http://host.robots.ox.ac.uk/pascal/VOC/voc{year}/"
# Data mirror:
_VOC_DATA_URL = "https://zenodo.org/record/5139598/files/"
_VOC_LABELS = (
    "trunk",
    "medium_grape_bunch",
    "tiny_grape_bunch"
)
_VOC_POSES = (
    "unspecified",
)


def _get_example_objects(annon_filepath):
  """Function to get all the objects from the annotation XML file."""
  with tf.io.gfile.GFile(annon_filepath, "r") as f:
    root = xml.etree.ElementTree.parse(f).getroot()

    # Disable pytype to avoid attribute-error due to find returning
    # Optional[Element]
    # pytype: disable=attribute-error
    size = root.find("size")
    width = float(size.find("width").text)
    height = float(size.find("height").text)

    for obj in root.findall("object"):
      # Get object's label name.
      label = obj.find("name").text.lower()
      # Get objects' pose name.
      pose = obj.find("pose").text.lower()
      is_truncated = (obj.find("truncated").text == "1")
      is_difficult = (obj.find("difficult").text == "1")
      bndbox = obj.find("bndbox")
      xmax = float(bndbox.find("xmax").text)
      xmin = float(bndbox.find("xmin").text)
      ymax = float(bndbox.find("ymax").text)
      ymin = float(bndbox.find("ymin").text)

      assert xmax > 0 or xmax < width, xmax
      assert xmin > 0 or xmin < width, xmin
      assert ymax > 0 or ymax < height, ymax
      assert ymin > 0 or ymin < height, ymin
      assert xmax - xmin > 0, "xmax: {}, xmin: {}, diff: {}".format(xmax, xmin, xmax - xmin)
      assert ymax - ymin > 0, "ymax: {}, ymin: {}, diff: {}".format(ymax, ymin, ymax - ymin)
      assert label in _VOC_LABELS, label

      yield {
          "label":
              label,
          "pose":
              pose,
          "bbox":
              tfds.features.BBox(ymin / height, xmin / width, ymax / height,
                                 xmax / width),
          "is_truncated":
              is_truncated,
          "is_difficult":
              is_difficult,
      }
    # pytype: enable=attribute-error


class VocConfig(tfds.core.BuilderConfig):
  """BuilderConfig for Voc."""

  def __init__(self,
               filenames=None,
               has_test_annotations=True,
               **kwargs):
    self.filenames = filenames
    self.has_test_annotations = has_test_annotations
    super(VocConfig, self).__init__(
        name="Aveleda",
        version=tfds.core.Version("1.0.0"),
        **kwargs)


class Aveleda(tfds.core.GeneratorBasedBuilder):
  """Dataset"""

  BUILDER_CONFIGS = [
      VocConfig(
          description=_VOC_CONFIG_DESCRIPTION,
          filenames={
              "dataset": "grape_trunk_dataset_split.zip",
          },
          has_test_annotations=True,
      ),
  ]

  def _info(self):
    return tfds.core.DatasetInfo(
        builder=self,
        description=_VOC_DESCRIPTION,
        features=tfds.features.FeaturesDict({
            "image":
                tfds.features.Image(),
            "image/filename":
                tfds.features.Text(),
            "objects":
                tfds.features.Sequence({
                    "label": tfds.features.ClassLabel(names=_VOC_LABELS),
                    "bbox": tfds.features.BBoxFeature(),
                    "pose": tfds.features.ClassLabel(names=_VOC_POSES),
                    "is_truncated": tf.bool,
                    "is_difficult": tf.bool,
                }),
            "labels":
                tfds.features.Sequence(
                    tfds.features.ClassLabel(names=_VOC_LABELS)),
            "labels_no_difficult":
                tfds.features.Sequence(
                    tfds.features.ClassLabel(names=_VOC_LABELS)),
        }),
        # disable_shuffling = True,
        homepage=_VOC_URL,
        citation=_VOC_CITATION,
    )

  def _split_generators(self, dl_manager):
    # paths = dl_manager.download_and_extract({
    #     k: os.path.join(_VOC_DATA_URL, v)
    #     for k, v in self.builder_config.filenames.items()
    # })
    paths = {"dataset": os.path.join(os.getcwd(), "aveleda/Aveleda")}
    return [
        tfds.core.SplitGenerator(
            name=tfds.Split.TEST,
            gen_kwargs=dict(data_path=paths["dataset"], set_name="test")),
        tfds.core.SplitGenerator(
            name=tfds.Split.TRAIN,
            gen_kwargs=dict(data_path=paths["dataset"], set_name="train")),
        tfds.core.SplitGenerator(
            name=tfds.Split.VALIDATION,
            gen_kwargs=dict(data_path=paths["dataset"], set_name="val")),
    ]

  def _generate_examples(self, data_path, set_name):
    """Yields examples."""
    set_filepath = os.path.join(
        data_path,
        os.path.normpath("ImageSets/Main/{}.txt".format(
            set_name)))
    load_annotations = (
        self.builder_config.has_test_annotations or set_name != "test")
    with tf.io.gfile.GFile(set_filepath, "r") as f:
      for line in f:
        image_id = line.strip()
        example = self._generate_example(data_path, image_id, load_annotations)
        yield image_id, example

  def _generate_example(self, data_path, image_id, load_annotations):
    image_filepath = os.path.join(
        data_path,
        os.path.normpath("JPEGImages/{}.jpg".format(
            image_id)))
    annon_filepath = os.path.join(
        data_path,
        os.path.normpath("Annotations/{}.xml".format(
            image_id)))
    if load_annotations:
      objects = list(_get_example_objects(annon_filepath))
      # Use set() to remove duplicates
      labels = sorted(set(obj["label"] for obj in objects))
      labels_no_difficult = sorted(
          set(obj["label"] for obj in objects if obj["is_difficult"] == 0))
    else:  # The test set of VOC2012 does not contain annotations
      objects = []
      labels = []
      labels_no_difficult = []
    return {
        "image": image_filepath,
        "image/id": image_id,
        "image/filename": image_id + ".jpg",
        "objects": objects,
        "labels": labels,
        "labels_no_difficult": labels_no_difficult,
    }